///<reference types="Cypress" />
import 'cypress-wait-until'
//////////////////// Task 1 ///////////////////////
describe('ExtraaEdge Test Cases', function () {
    
    before(function () {   
        cy.BaseUrl()
        })
    beforeEach(function(){
        cy.viewport(1640,1350)
        })
    context('1. Login flow with validations checking', function() {    
        it('Verify that page contains login form when hitting baseurl', ()=>{
            
            cy.get('.loginForm').should('be.exist')
            cy.get('.loginForm').should('be.visible')
        })

        it('Login with valid Credentials then system not generate a validation message ', () => {

            cy.login({ email: Cypress.env('valid-username'), password: Cypress.env('valid-password') })
            cy.waitUntil(() => true)
            cy.logout()
            
        })
        it('Login with valid username and invalid Password then system generate a valid error message ', () => {

            cy.login({ email: Cypress.env('valid-username'), password: Cypress.env('invalid-password') })
            cy.get('.error').then((el)=> {
                assert.include(el.text(), 'Invalid username or password.'); 
            });
            cy.reload()  
        })  
        it('Login with invalid username and valid Password then system generate a valid error message', () => {

            cy.login({ email: Cypress.env('invalid-username'), password: Cypress.env('valid-password') })
            cy.get('.error').then((el)=> {
                assert.include(el.text(), 'Some Error Occured, Please try again later'); 
                });  
            cy.reload()     
        })   
        it('Login with empty username and empty password then system generate a required error message', () => {

            cy.get('input[name=userName]').clear()
            cy.get('input[name=password]').clear()
            cy.get('button').contains('LOGIN').click()
            cy.get('.error').should(($lis) => {
                expect($lis.eq(1)).to.contain('Please enter Username')    
                expect($lis.eq(3)).to.contain('Please enter Password') 
            })
        })
    })

    //////////////////// Task 2 ///////////////////////
    context('2. After login dashboard flow click on Add Lead button', function () {

        it('After successfully login verify that On dashboard Add Lead button is present', ()=>{

            cy.login({ email: Cypress.env('valid-username'), password: Cypress.env('valid-password') })
            cy.waitUntil(() => true)
            cy.get('#add-new-lead > img').should('be.exist')
            cy.get('#add-new-lead > img').should('be.visible')
        })
        it('After successfully login verify that click on Add Lead button ', ()=>{

            cy.get('#add-new-lead > img').click()
            cy.get('.tabbing').should('be.exist')
            cy.logout()
        })    
    })    

//////////////////// Task 3 ///////////////////////
    context('3. Fill up all the mandatory fields and submit the form', function () {

        it('After successfully login verify that On dashboard Add Lead button is present', ()=>{

            cy.login({ email: Cypress.env('valid-username'), password: Cypress.env('valid-password') })
            cy.waitUntil(() => true)
            cy.get('#add-new-lead').should('be.exist')
            cy.get('#add-new-lead').should('be.visible')
            cy.logout()
        })
        it('fill up new lead form with valid details', ()=>{

            cy.login({ email: Cypress.env('valid-username'), password: Cypress.env('valid-password') })
            cy.waitUntil(() => true)
            cy.get('#add-new-lead').click({ force: true })
            cy.get('#leadDetailsTabForm').should('be.exist').wait(2000)
            cy.log("lead email",Cypress.env('leademail'))
            cy.Addlead({ email: Cypress.env('leademail'), Department: Cypress.env('leadDepartment'),source: Cypress.env('leadsource'),reason: Cypress.env('leadreason'),interviewcomment: Cypress.env('leadinterviewcomment') })
            cy.waitUntil(() => cy.get('.toastr-middle-container > .title').should('be.visible'))
            cy.logout()
        })    
    })

//////////////////// Task 4 ///////////////////////
    context('4. The newly added lead should be visible in the list', function () {

        it('Verify that newly added lead should be visible', ()=>{   

            cy.login({ email: Cypress.env('valid-username'), password: Cypress.env('valid-password') })
            cy.waitUntil(() => true)
            cy.wait(3000)
            cy.get('#accordiontab-0-0 > .panel > .panel-heading > .accord-row > .second-column > .copyEmailIcon > .small-text').should('be.visible').then(($el) => {
                expect($el).to.contain(Cypress.env('leademail'))
            })
        })
    })        
})