// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })
Cypress.Commands.add("login", (user) => { 
    cy.get('input[name=userName]').type(user.email)
    cy.get('input[name=password]').type(user.password)
    cy.get('button').contains('LOGIN').click({ force: true })
    cy.wait(3000)
})

Cypress.Commands.add('logout', () => {
    cy.get('.avatar > img').click({force: true} )
    cy.get('a[title=Logout').click({force: true} );
});

Cypress.Commands.add('BaseUrl', () => {
    cy.visit(Cypress.env('baseUrl'))
    
});
Cypress.Commands.add("Addlead", (lead) => { 
    cy.get('input[id=email]').type(lead.email, { force: true })
    cy.get('#entity1').type(lead.Department, { force: true }).type('{enter}')
    cy.get('#leadSource').type(lead.source, { force: true }).type('{enter}')
    cy.get('#leadReason').clear()
    cy.get('#leadReason').type(lead.reason, { force: true }).type('{enter}')
    cy.get('#activityComment').type(lead.interviewcomment, { force: true })
    cy.get('button').contains('ADD LEAD').click({ timeout: 2000 })
})